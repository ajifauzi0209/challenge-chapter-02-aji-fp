package com.kazakimaru.ch02_ajifauzipangestu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn_activoty2 = findViewById<Button>(R.id.btn_main_activity2)

        callButtonActivity1()
        callButtonActivity2()
    }


    private fun callButtonActivity1() {
        val btn_activity1 = findViewById<Button>(R.id.btn_main_activity1)
        btn_activity1.setOnClickListener {
            // Pindah Activity
            // Cara pertama
//            val intent = Intent(this, ActivityPertama::class.java)
//            startActivity(intent)

            // Cara kedua
            startActivity(Intent(this, ActivityPertama::class.java))
        }
    }

    private fun callButtonActivity2() {
        val btn_activity2 = findViewById<Button>(R.id.btn_main_activity2)
        btn_activity2.setOnClickListener {
            // Pindah Activity
            // Cara pertama
//            val intent = Intent(this, ActivityPertama::class.java)
//            startActivity(intent)

            // Cara kedua
            startActivity(Intent(this, ActivityKedua::class.java))
        }
    }
}
